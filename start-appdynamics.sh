#!bin/bash

# Sample Docker start script for the AppDynamics Standalone Machine Agent
# This is provided for illustration purposes only
 
# Enable using container ID as host ID when the pod has more than one containers
# MA_PROPERTIES+=" -Dappdynamics.docker.container.containerIdAsHostId.enabled=true"
# Start Machine Agent
$MACHINE_AGENT_HOME/bin/machine-agent $MA_PROPERTIES start