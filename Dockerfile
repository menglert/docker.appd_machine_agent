# Sample Dockerfile for the AppDynamics Standalone Machine Agent
# This is provided for illustration purposes only, for full details 
# please consult the product documentation: https://docs.appdynamics.com/

FROM openjdk:8-jre-slim

ENV MACHINE_AGENT_HOME /opt/appdynamics/machine-agent/
# Add AppDynamics Machine Agent
# Download the Machine Agent (zip) from https://download.appdynamics.com
# Rename it to machine-agent.zip and place it along with the Dockerfile 
ADD machine-agent.zip /tmp/ 

# Include start script to configure and start MA at runtime
ADD start-appdynamics.sh $MACHINE_AGENT_HOME

# Install required packages
RUN apt-get update \
    &&  apt-get upgrade -y \
    &&  apt-get install -q -y --fix-missing unzip \
    &&  mkdir -p $MACHINE_AGENT_HOME \
    &&  unzip -oq /tmp/machine-agent.zip -d $MACHINE_AGENT_HOME \
    &&  chmod 744 $MACHINE_AGENT_HOME/start-appdynamics.sh \
    &&  apt-get remove --purge -q -y unzip \
    &&  apt-get autoremove -q -y \
    &&  apt-get clean -q -y \
    &&  rm -rf /tmp/*

# Configure and Run AppDynamics Machine Agent
CMD [ "/bin/bash", "-c", "$MACHINE_AGENT_HOME/start-appdynamics.sh" ]